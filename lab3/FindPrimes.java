public class FindPrimes {
    public static void main(String[] args) {
        //print the prime numbers until the given number

        //get the number
        int given = Integer.parseInt(args[0]);
        //for each number less than the given
        for (int number = 2; number < given; number++) {
            boolean isPrime = true;

            //for each division less than number
            for (int divisor = 2; divisor < number; divisor++) {
                //if number is divisible by the number
                if (number % divisor == 0) {
                    isPrime = false;
                    break;
                }
            }
            //if it is prime

            if (isPrime)
            //print the number
                System.out.print(number +". ");
        }
    }
}
