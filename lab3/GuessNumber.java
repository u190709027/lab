import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); //Read the user input

		int count = 0;

		while (guess != -1 && guess != number) {
			System.out.println("Sorry!");
			if (guess < number) {
				System.out.println("Too Low!");
			}else if (guess > number) {
				System.out.println("Too High!");
			}
			System.out.print("Type -1 to quit or guess another number: ");
			guess = reader.nextInt(); //Read the user input
			count++;
		}
		
		if (guess == number) {
			System.out.println("Congratulations!");
		}else {
			System.out.println("Sorry! The correct number was: " + number);
		}

		System.out.println("You won after " + count + " attempts!");

		reader.close(); //Close the resource before exiting
	}
	
	
}