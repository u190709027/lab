package lab.lab6;

public class Circle {
    int radius;
    int center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area() {
        return Math.PI * radius * radius;
    }
    
    public boolean intersect(Circle c) {
        return (radius + c.radius >= center.distance(c.center));
    }
}
