package lab.lab6;

public class Point {
    int xCoord = 5;
    int yCoord = 5;

    public Point(){

    }

    public Point(int xy) {
        xCoord = xy;
        yCoord = xy;
    }

    public Point(int x, int y) {
        xCoord = x;
        yCoord = y;
    }

    public double distance(Point p) {
        return Math.sqrt((xCoord - p.xCoord) * (xCoord - p.xCoord) + (yCoord - p.yCoord) * (yCoord - p.yCoord));
    }
}
