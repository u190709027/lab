package lab.lab6;

import org.w3c.dom.css.Rect;

public class TestRectangle {
    public static void main(String[] args) {
        //Point p = new Point(3,7); // can also be defined below
        Rectangle r = new Rectangle(5,6, new Point (3,7));
        System.out.println("area = " + r.area());
        System.out.println("perimeter = " r.perimeter());

        Point[] corners = r.corners();
        for (int i = 0; i < corners.length; i++) {
            System.out.println("x = " corners[i].xCoord + " y = " corners[i].yCoord);
        }

        Rectangle r2 = new Rectangle(7,9, new Point(3,10));
        System.out.println("area = " + r2.area());
    }
}
